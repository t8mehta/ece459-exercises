use std::sync::mpsc;
use std::thread;
use std::time::Duration;
static N: i32 = 1000;  // number of threads to spawn
// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    for i in 0..N{
        let tx1 = tx.clone();

        thread::spawn(move || {
            tx1.send(format!("{}{}","hi",i)).unwrap();
            println!("thread {} finished",i);
            thread::sleep(Duration::from_secs(1));
        });
    }

    for received in rx {
        println!("Got: {}", received);
    }
}
