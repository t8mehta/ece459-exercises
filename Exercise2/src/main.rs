// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let mut fibb_seq: Vec<u32> = Vec::new();
    for i in 0..n+1 {
        if i == 0{
            fibb_seq.push(i);
        }
        else if i == 1{
            fibb_seq.push(i);
        }
        else{
            let size1 = (i-1) as usize;
            let size2 = (i-2) as usize;

            let prevone: &u32 = &fibb_seq[size1];
            let prevtwo: &u32 = &fibb_seq[size2];
            fibb_seq.push(prevone + prevtwo)
        }
    }  
    return fibb_seq[fibb_seq.len()-1];
}

fn main() {
    println!("{}", fibonacci_number(10));
}
